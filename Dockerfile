FROM nginx

RUN apt update
RUN apt upgrade
RUN apt install vim -y

COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
